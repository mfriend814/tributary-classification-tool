#Setup.
##Import necessary modules.  
import arcpy

##NOTE: do not set the workspace, because you don't want to set the workspace in a tool, because then the tool would only work in that folder.

#Create the Toolbox class, which is an object that can have properties and methods.
class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        ##This is the name of the toolbox when it loads into ArcToolbox.
        self.label = "Tributary Toolbox"
        ##This is a list of the tools in the toolbox.
        self.alias = "Slope, Lithology, and Basin Area"

        ##List of tool classes associated with this toolbox.
        self.tools = [Slope]
	self.tools = [Lithology]
	self.tools = [Basin Area]
	self.tools = [Random_Selection]

#Within the toolbox there are 3 separate tools: slope, lithology, and basin area. Users can choose which to run.
##Slope Tool 
class Slope(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Slope"
        self.description = "Classify tributaries of a given stream reach by slope."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        ###First parameter: the input raster of the stream reach, a hydrologically conditioned DEM.
        param0 = arcpy.Parameter(
            displayName = 'Input reach',
            name = 'input_reach',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
        ###Second parameter: the output raster with slopes.
        param1 = arcpy.Parameter(
            displayName = 'Reach slopes'
            name = 'reach_slope',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Output')
        ###Return a list of parameters. 
        params = [param0, param1]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        ##Check out ArcGIS Spatial Analyst extension license
        arcpy.CheckOutExtension('Spatial')
        #Get parameters.
        input_reach = parameters[0].valueAsText
        reach_slope = parameters[1].valueAsText
        #Calculate slope of the input reach and save to output.
        arcpy.outSlope = Slope(in_raster = 'dem,' output_measurement = 'DEGREE', z_factor = "", method = 'GEODESIC', zUnit = 'METER')
        #Save.
        outSlope.save(r'outputlocation.tif')
        return
        
##Lithology Tool.
class Lithology(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Lithology"
        self.description = "Classify the tributaries of a given stream reach by lithology."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        ###First parameter: the input raster of the lithology.
        param0 = arcpy.Parameter(
            displayName = 'Lithology',
            name = 'lithology',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')
        ###Second parameter: the output raster with lithology.
        param1 = arcpy.Parameter(
            displayName = 'Lithology'
            name = 'output_lithology',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')
        ###Return a list of parameters. 
        params = [param0, param1]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        ##Check out ArcGIS Spatial Analyst extension license
        arcpy.CheckOutExtension('Spatial')
        #Get parameters.
        input_lithology = parameters[0].valueAsText
        output_lithology = parameters[1].valueAsText
        #Calculate slope of the input reach and save to output.       
        arcpy.sa.ClassifyRaster = ClassifyRaster(in_raster = 'example_input_lithology,' in_classifier_definition = 'example.ecd')
        #Save.
        ClassifyRaster.save(r'outputlocation.tif')
        return
        
##Basin Area Tool.
class Basin_Area(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Basin Area"
        self.description = "Classify the tributaries of a given stream reach by basin area."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        ###First parameter: the hydrologically conditioned DEM.
        param0 = arcpy.Parameter(
            displayName = 'Basin Area Input',
            name = 'input_basin_area',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
        ###Second parameter: the output value of the basin area.
        param1 = arcpy.Parameter(
            displayName = 'Basin Area Output'
            name = 'output_basin_area',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Output')
        ###Return a list of parameters. 
        params = [param0, param1]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        ##Check out ArcGIS Spatial Analyst extension license
        arcpy.CheckOutExtension('Spatial')
        #Get parameters.
        input_basin_area = parameters[0].valueAsText
        output_basin_area = parameters[1].valueAsText
        #Calculate basin area of the tributary and save to output.       
        arcpy.sa.Basin = Basin(in_flow_direction_raster = 'fdr,')
        #Save.
        Basin.save(r'outputlocation.tif')
        return
    
##Random selection tool. 
   class Random_Selection(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Random tributary selection"
        self.description = "Randomly select a certain number of tributaries for which the parameters were calculated."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        ###First parameter: Slope results
        param0 = arcpy.Parameter(
            displayName = 'Slope results',
            name = 'reach_slope',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
        ###Second parameter: Lithology output.
         param1 = arcpy.Parameter(
            displayName = 'Lithology results'
            name = 'output_lithology',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')
         ###Third parameter: Basin area output.
        param2 = arcpy.Parameter(
            displayName = 'Basin Area results'
            name = 'output_basin_area',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
         ###Fourth parameter: the random tributary selection outputs
        param1 = arcpy.Parameter(
            displayName = 'Random tributary selection'
            name = 'random_tribs',
            datatype = 'GPPolygon',
            parameterType = 'Required',
            direction = 'Output')
        ###Return a list of parameters. 
        params = [param0, param1, param2, param3]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        #Get parameters.
        reach_slope = parameters[0].valueAsText
        output_lithology = parameters[1].valueAsText
        outut_basin_area = parameters[2].valueAsText
        #Select random tributaries.       
        arcpy.randomGenerator = 'reach_slope', 'output_lithology', 'output_basin_area'
        #Calculate a random number using the arcgis.rand() function
        random_tribs = arcpy.CalculateValue_management("arcgis.rand('normal 0.0 10.0')")
        #Get the value from the result object from CalculateValue and print 
        randomValue = float(result[0])
        print(randomValue)
        #Save.
        Random_Selection.save(r'outputlocation.tif')
        return 

# Tributary classification tool

The goal of this project is to create a tool for classifying tributaries of a given river or reservoir reach. Tributaries will be classified by lithology, basin area, slope, and valley width. Random representative tributaries will then be selected. 